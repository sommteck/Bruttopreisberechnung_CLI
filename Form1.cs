using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bruttopreisberechnung_CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            double nettopreis, brutto;
            char ustkz;

            Console.Write("Nettopreis: ");
            nettopreis = Convert.ToDouble(Console.ReadLine());

            while (nettopreis > 0)
            {
                Console.Write("Umsatzsteuerkennzeichen (V/N): ");
                ustkz = Convert.ToChar(Console.ReadLine());
                if (ustkz == 'N')
                {
                    brutto = nettopreis * 1.19;
                    Console.WriteLine(brutto);
                }
                else
                    if (ustkz == 'V')
                    {
                        brutto = nettopreis * 1.07;
                        Console.WriteLine(brutto);
                    }
                    else
                        Console.WriteLine("Nur 'N' oder 'V' eingeben!");

                Console.Write("Nettopreis: ");
                nettopreis = Convert.ToDouble(Console.ReadLine());
            }
            Console.WriteLine("... weiter mit beliebiger Taste!");
            Console.ReadKey();
        }
    }
}
